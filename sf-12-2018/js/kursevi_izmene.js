var baza = 'https://sf-12-2018-default-rtdb.firebaseio.com/kursevi.json';

var kurseviIzmeneBaza;

function kurseviIzmenePodaci(i) {
    var request = new XMLHttpRequest();

    request.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (this.status == 200) {
                kurseviIzmeneBaza = JSON.parse(this.responseText);
                kursIzmeneGenerisanje(i)
            } else {
                alert('Greska prilikom ucitavanja podataka.');
            }
        }
    }

    request.open('GET', baza);
    request.send();
}

function kursIzmenaKartica(slikaKursa, autorKursa, idKursa, datumIzmeneKursa, cenaKursa, brojLekcijaKursa, jezikKursa, brojKorisnikaKursa, prosecnaOcenaKursa, sertifikovanKurs) {

    return '<div class="form-group"><img src=" '+ slikaKursa +' " class="card-img-top" alt="slika"><label for="author"> Author </label><input type="text" class="form-control" id="autor" value="' +
        autorKursa + '"></div><div class = "form-group"><label for = "ID"> ID of the course </label><input type = "number" class = "form-control" id = "ID" value = "' +
        idKursa + '" readonly></div><div class = "form-group"><label for = "datumIzmene"> Date of the last update </label><input type = "date" class = "form-control" id = "datumIzmene" value = "' +
        datumIzmeneKursa + '" readonly></div><div class = "form-group"><label for = "cena" > Price </label><input type = "text" class = "form-control" id = "cena" value = "' +
        cenaKursa + '$"></div><div class = "form-group"><label for = "lekcije"> Lections </label><input type = "text" class = "form-control" id = "lekcije" value = "' +
        brojLekcijaKursa + '"></div><div class = "form-group"><label for = "kategorija" > Category </label><select id = "kategorija" class = "form-control">' +
        '<option selected></option><option> Objective programming </option><option> Web programming </option><option> Algorithms </option><option> Database </option></select></div>' +
        '<div class = "form-group"><label for = "jezik"> Language </label><input type = "text" class = "form-control" id = "jezik" value = "' +
        jezikKursa + '"></div><div class = "form-group"><label for = "brojKorisnika"> People who passed the course </label><input type = "text" class = "form-control" id = "brojKorisnika" value = "' +
        brojKorisnikaKursa + '" readonly></div><div class = "form-group"><label for = "prosecnaOcena" > Avg rating </label><input type = "text" class = "form-control" id = "prosecnaOcena" value = "' +
        prosecnaOcenaKursa + '" readonly></div><div class = "form-group"><label for = "sertifikat" > Sertified ? </label><input type = "text" class = "form-control" id = "sertifikat" value = "' +
        sertifikovanKurs + '"></div>' 
        //'<button class="btn btn-primary" onclick="proslediIzmeneKursa()" style="background-color: rgba(0, 0, 255, 0.705);">Update</button>'

}

function kursIzmeneGenerisanje(i) {
    var izmeneArr = '';

    var kurseviArr = [
        kurseviIzmeneBaza[Object.keys(kurseviIzmeneBaza)[i]].slika,
        kurseviIzmeneBaza[Object.keys(kurseviIzmeneBaza)[i]].autor,
        kurseviIzmeneBaza[Object.keys(kurseviIzmeneBaza)[i]].id,
        kurseviIzmeneBaza[Object.keys(kurseviIzmeneBaza)[i]].datumIzmene,
        kurseviIzmeneBaza[Object.keys(kurseviIzmeneBaza)[i]].cena,
        kurseviIzmeneBaza[Object.keys(kurseviIzmeneBaza)[i]].brojLekcija,
        kurseviIzmeneBaza[Object.keys(kurseviIzmeneBaza)[i]].jezik,
        kurseviIzmeneBaza[Object.keys(kurseviIzmeneBaza)[i]].brojKorisnika,
        kurseviIzmeneBaza[Object.keys(kurseviIzmeneBaza)[i]].prosecnaOcena,
        kurseviIzmeneBaza[Object.keys(kurseviIzmeneBaza)[i]].sertifikovan
    ]
    izmeneArr = kursIzmenaKartica(kurseviArr[0], kurseviArr[1], kurseviArr[2], kurseviArr[3], kurseviArr[4], kurseviArr[5],
        kurseviArr[6], kurseviArr[7], kurseviArr[8], kurseviArr[9]);
    document.getElementById("polja-izmene").innerHTML = izmeneArr
}

function proslediIzmeneKursa() {
    var autor = document.getElementById('autor').value;
    var cena = document.getElementById('cena').value;
    var lekcije = document.getElementById('lekcije').value;
    var kategorija = document.getElementById('kategorija').value;
    var jezik = document.getElementById('jezik').value;
    var sertifikat = document.getElementById('sertifikat').value;

    if(autor == '' || cena == '' || lekcije == '' || kategorija == '' || jezik == '' || sertifikat == '') {
        alert("Nisu uneti svi podaci");
    } else {
        alert("Uspesno ste izmenili kurs");
        window.open('kursevi.html', "_self");
    }
}

function listaIndeksa(listaIndeksaKurseva) {
    console.log(listaIndeksaKurseva);
    window.open("kursevi_detalji.html?listaIndeksaK=" + listaIndeksaKurseva, "_self");
}


