var baza = 'https://sf-12-2018-default-rtdb.firebaseio.com/korisnici.json';
var korisniciDetaljiBaza

function korisniciDetaljiPodaci(i) {
    var request = new XMLHttpRequest();

    request.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (this.status == 200) {
                korisniciDetaljiBaza = JSON.parse(this.responseText);
                generisanjeKorisnikaDetalji(i)
            } else {
                alert('Greska prilikom ucitavanja podataka.');
            }
        }
    }

    request.open('GET', baza);
    request.send();
}

function karticaKorisnikaDetalji(imeKorisnika, prezimeKorisnika, emailKorisnika, korisnickoImeKorisnika, datumRodjenjaKorisnika, adresaKorisnika, telefonKorisnika, lozinkaKorisnika) {

    return '<li class="list-group-item">Name: <b>' + imeKorisnika + '</b></li>' +
        '<li class="list-group-item">Surname: <b>' + prezimeKorisnika + '</b></li>' +
        '<li class="list-group-item">Email: <b>' + emailKorisnika + '</b></li>' +
        '<li class="list-group-item">User name: <b>' + korisnickoImeKorisnika + '</b></li>' +
        '<li class="list-group-item">Date of birth: <b>' + datumRodjenjaKorisnika + '</b></li>' +
        '<li class="list-group-item">Adress: <b>' + adresaKorisnika + '</b></li>' +
        '<li class="list-group-item">Mobile number: <b>' + telefonKorisnika + '</b></li>'
}

function generisanjeKorisnikaDetalji(i) {
    var detaljiArr = '';
    document.getElementById('korisnik-detalji').innerHTML = '';

    var korisnikArr = [
        korisniciDetaljiBaza[Object.keys(korisniciDetaljiBaza)[i]].ime,
        korisniciDetaljiBaza[Object.keys(korisniciDetaljiBaza)[i]].prezime,
        korisniciDetaljiBaza[Object.keys(korisniciDetaljiBaza)[i]].email,
        korisniciDetaljiBaza[Object.keys(korisniciDetaljiBaza)[i]].korisnickoIme,
        korisniciDetaljiBaza[Object.keys(korisniciDetaljiBaza)[i]].datumRodjenja,
        korisniciDetaljiBaza[Object.keys(korisniciDetaljiBaza)[i]].adresa,
        korisniciDetaljiBaza[Object.keys(korisniciDetaljiBaza)[i]].telefon,
        korisniciDetaljiBaza[Object.keys(korisniciDetaljiBaza)[i]].lozinka
    ]

    detaljiArr = karticaKorisnikaDetalji(korisnikArr[0], korisnikArr[1], korisnikArr[2], korisnikArr[3], korisnikArr[4],
        korisnikArr[5], korisnikArr[6], korisnikArr[7]);
    document.getElementById("korisnik-detalji").innerHTML = detaljiArr
}

function pozoviGenerisanjeKorisnika(i) {
    document.getElementById('korisniciOsnova').style.display = 'none';
    document.getElementById('detaljiKorisnika').style.display = 'block';
    document.getElementById('izmenaKorisnika').style.display = 'none';
    generisanjeKorisnikaDetalji(i);
}

function deaktivirajKorisnika() {
    if (confirm("Deaktiviraj korisnika?")) {
        window.open("korisnici.html", "_self");
        alert("Uspesno obrisan korisnik!")
    }
}

function izmeneKorisnikaPozivanje(i) {
    window.open("korisnici_izmene.html?indeksKorisnikaKursa=" + i, "_self");
}