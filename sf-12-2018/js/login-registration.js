var loginForm = document.getElementById('loginForm');
var registrationForm = document.getElementById('registrationForm');

loginForm.addEventListener('submit', function (e) {
    e.preventDefault();

    var username = document.getElementById('user-name').value.trim();
    var password = document.getElementById('user-password').value.trim();

    if (username == '' || password == '') {
        alert('Morate uneti sve podatke za prijavu.');
    } else {
        var request = new XMLHttpRequest();

        request.onreadystatechange = function () {
            if (this.readyState == 4) {
                if (this.status == 200) {
                    var users = JSON.parse(request.responseText);
                    console.log(users);
                    var name = '';
                    for (let email in users) {
                        var user = users[email];
                        console.log(user);
                        if (user.korisnickoIme == username && user.lozinka == password) {
                            name = user.ime;
                            break;
                        }
                    }
                    if (name == '') {
                        alert('Neispravni uneti podaci.');
                    } else {
                        window.open('kursevi.html', "_self");
                        alert("Uspesno ste ulogovali")
                    }
                } else {
                    alert('GRESKA: ' + this.status);
                }
            }
        };
        var url = loginForm.getAttribute('action');
        request.open('GET', url);
        request.send();
    }
});

registrationForm.addEventListener('submit', function (e) {
    e.preventDefault();

    var adress = document.getElementById('adress').value.trim();
    var email = document.getElementById('email').value.trim();
    var dateBirth = document.getElementById('date-birth').value.trim();
    var name = document.getElementById('name').value.trim();
    var userName = document.getElementById('username').value.trim();
    var password = document.getElementById('password').value.trim();
    var surname = document.getElementById('surname').value.trim();
    var contact = document.getElementById('contact').value.trim();

    var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if (adress == '' || email == '' || name == '' || userName == '' || password == '' || surname == '' || contact == '' || dateBirth == '') {
        alert('Morate uneti sve podatke za prijavu!');
    } else if (!email.match(validRegex)) {
        alert('Nevalidna email adresa!');
    } else {
        alert('Uspesno ste se registrovali!')
        window.open('kursevi.html', "_self");
    }
});