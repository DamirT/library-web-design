var baza = 'https://sf-12-2018-default-rtdb.firebaseio.com/kursevi.json';
var kurseviDetaljiBaza;

function shoppingKartKursa(i) {
    var request = new XMLHttpRequest();

    request.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (this.status == 200) {
                kurseviDetaljiBaza = JSON.parse(this.responseText);
                shoppingKartGenerisanje(i)
            } else {
                alert('Greska prilikom ucitavanja podataka.');
            }
        }
    }

    request.open('GET', baza);
    request.send();
}

function kursDetaljiKartica(imeSlike, nazivKursa, autorKursa, cenaKursa,kategorijaKursa) {
        return '<div class="col-md-6 col-sm-12">' +
                '<div class="card" style="width:400px">' +
                    '<img class="card-img-top" src="' + imeSlike + '" alt="Card image' +
                        'style="width:100%">' +
                    '<div class="card-body">' +
                        '<h4 class="card-title">"' + nazivKursa + '"</h4>' +
                        '<p class="card-text">"' + autorKursa + '"</p>' +
                        '<p class="card-text">"' + cenaKursa + '"$</p>' +
                        '<p class="card-text">"' + kategorijaKursa + '"</p>' +
                        '<a href="#" class="btn btn-primary" onclick="obrisiShoppingCart()">Delete</a>' +
                    '</div>' +
                '</div>' +
            '</div>' +
        '</div>'
}

    {/* return '<div class="card-body"><img src="' + imeSlike +'" class="card-img-top" alt="slika">' +
    '<h5 class="card-title" style="height: 40px;">' +
        nazivKursa + '</h5><p class="card-text" style="height: 130px; overflow: scroll;">' +
        opisKursa + '</p></div><ul class="list-group list-group-flush">' +
        '<li class="list-group-item">Autor: <b>' + autorKursa + '</b></li>' +
        '<li class="list-group-item">ID of the course: <b>' + idKursa + '</b></li>' +
        '<li class="list-group-item">Date of last update: <b>' + datumIzmeneKursa + '</b></li>' +
        '<li class="list-group-item">Price: <b>' + cenaKursa + '$</b></li>' +
        '<li class="list-group-item">Lections: <b>' + brojLekcijaKursa + '</b></li></li>' +
        '<li class="list-group-item">Category: <b>' + kategorijaKursa + '</b></li>' +
        '<li class="list-group-item">Language: <b>' + jezikKursa + '</b></li>' +
        '<li class="list-group-item"><b><p><span style="color: rgb(4, 150, 4);">' + brojKorisnikaKursa + '</span> people passed the course</p></b></li>' +
        '<li class="list-group-item"><i class="far fa-star"></i>Avg rating: <b>' + prosecnaOcenaKursa + '</b></li>' +
        '<li class="list-group-item"><b> Sertified: ' + sertifikovanKurs + '</b></li></ul>' */}


function shoppingKartGenerisanje(i) {
    var detaljiArr = '';

    var kurseviArr = [
        kurseviDetaljiBaza[Object.keys(kurseviDetaljiBaza)[i]].slika,
        kurseviDetaljiBaza[Object.keys(kurseviDetaljiBaza)[i]].naziv,
        kurseviDetaljiBaza[Object.keys(kurseviDetaljiBaza)[i]].autor,
        kurseviDetaljiBaza[Object.keys(kurseviDetaljiBaza)[i]].cena,
        kurseviDetaljiBaza[Object.keys(kurseviDetaljiBaza)[i]].kategorija
    ]
    detaljiArr = kursDetaljiKartica(kurseviArr[0], kurseviArr[1], kurseviArr[2], kurseviArr[3], kurseviArr[4]);
    document.getElementById("gen-shopp-cart").innerHTML = detaljiArr
}

function obrisiShoppingCart() {
    if (confirm("Obrisi kurs iz korpe?")) {
        alert("Uspesno obrisan kurs");
    }
}

function karticaKorpeKursa(indeks) {
        console.log(indeks);
}   

function kupiKurs() {
    alert("Kurs uspesno kupljen!");
}