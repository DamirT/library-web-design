var baza = 'https://sf-12-2018-default-rtdb.firebaseio.com/kursevi.json';
var pretraga = document.getElementById('pretraga');
var kurseviBaza;

function kurseviPodaci() {
    var request = new XMLHttpRequest();

    request.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (this.status == 200) {
                kurseviBaza = JSON.parse(this.responseText);
                kurseviGenerisanje(Object.keys(kurseviBaza).length);
            } else {
                alert('Greska prilikom ucitavanja podataka.');
            }
        }
    }

    request.open('GET', baza);
    request.send();
}

function kursKartica(imeSlike, nazivKursa, opisKursa, autorKursa, kategorijaKursa, jezikKursa, cenaKursa,
    brojKorisnikaKursa, prosecnaOcenaKursa, sertifikovanKurs, indexKursa) {
    return '<div class="col-md-3 col-sm-12"><main><div class="card" style="width: 19rem;"><img src="' +
        imeSlike + '" class="card-img-top" alt="slika"><div class="card-body"><h5 class="card-title" style="height: 40px;">' +
        nazivKursa + '</h5><p class="card-text"  style="height: 110px; overflow: scroll;">' +
        opisKursa + '</p></div> ' +
        '<ul class = "list-group list-group-flush">' +
        '<li class = "list-group-item"> <b>' + autorKursa + '</b></li> ' +
        '<li class = "list-group-item" > <b>' + kategorijaKursa + '</b></li> ' +
        '<li class = "list-group-item" > <b> ' + jezikKursa + ', ' + cenaKursa + ' $,<br/> ' +
        '<p><span style = "color: rgb(4, 150, 4);"> ' + brojKorisnikaKursa +
        ' </span> people passed the course</p></b></li><li class = "list-group-item"> <i class = "far fa-star"></i>Avg rating: <b>' +
        prosecnaOcenaKursa + '<b></li><li class = "list-group-item"><b> Sertified: ' +
        sertifikovanKurs + ' </b></li > ' +
        '</ul><div class = "card-body"><button class = "btn btn-primary"  onclick="detaljiKursaPozivanje(' + indexKursa + ')"> Details </button></div></div></main></div>'
}

function kurseviGenerisanje(brojKurseva) {
    var coursesArr = '';
    var itr = 0

    for (var i = 0; i < brojKurseva; i++) {
        var kurseviArr = [
            kurseviBaza[Object.keys(kurseviBaza)[i]].slika,
            kurseviBaza[Object.keys(kurseviBaza)[i]].naziv,
            kurseviBaza[Object.keys(kurseviBaza)[i]].opis,
            kurseviBaza[Object.keys(kurseviBaza)[i]].autor,
            kurseviBaza[Object.keys(kurseviBaza)[i]].kategorija,
            kurseviBaza[Object.keys(kurseviBaza)[i]].jezik,
            kurseviBaza[Object.keys(kurseviBaza)[i]].cena,
            kurseviBaza[Object.keys(kurseviBaza)[i]].brojKorisnika,
            kurseviBaza[Object.keys(kurseviBaza)[i]].prosecnaOcena,
            kurseviBaza[Object.keys(kurseviBaza)[i]].sertifikovan
        ]
        coursesArr += kursKartica(kurseviArr[0], kurseviArr[1], kurseviArr[2], kurseviArr[3], kurseviArr[4],
            kurseviArr[5], kurseviArr[6], kurseviArr[7], kurseviArr[8], kurseviArr[9], i);
        itr++;
        if (itr == 4 || i == (brojKurseva - 1)) {
            document.getElementById("cards").innerHTML += '<div class="row">' + coursesArr + '</div>'
            coursesArr = '';
            itr = 0
        }
    }
}

function detaljiKursaPozivanje(i) {
    window.open("kursevi_detalji.html?indeksKarticeKursa=" + i, "_self");
}


    