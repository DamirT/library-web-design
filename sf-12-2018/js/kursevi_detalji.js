var baza = 'https://sf-12-2018-default-rtdb.firebaseio.com/kursevi.json';
var kurseviDetaljiBaza;



function kurseviDetaljiPodaci(i) {
    var request = new XMLHttpRequest();

    request.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (this.status == 200) {
                kurseviDetaljiBaza = JSON.parse(this.responseText);
                kursDetaljiGenerisanje(i)
            } else {
                alert('Greska prilikom ucitavanja podataka.');
            }
        }
    }

    request.open('GET', baza);
    request.send();
}

function kursDetaljiKartica(imeSlike, nazivKursa, opisKursa, autorKursa, idKursa, datumIzmeneKursa, cenaKursa, brojLekcijaKursa,
    kategorijaKursa, jezikKursa, brojKorisnikaKursa, prosecnaOcenaKursa, sertifikovanKurs) {

    return '<div class="card-body"><img src="' + imeSlike +'" class="card-img-top" alt="slika">' +
    '<h5 class="card-title" style="height: 40px;">' +
        nazivKursa + '</h5><p class="card-text" style="height: 130px; overflow: scroll;">' +
        opisKursa + '</p></div><ul class="list-group list-group-flush">' +
        '<li class="list-group-item">Autor: <b>' + autorKursa + '</b></li>' +
        '<li class="list-group-item">ID of the course: <b>' + idKursa + '</b></li>' +
        '<li class="list-group-item">Date of last update: <b>' + datumIzmeneKursa + '</b></li>' +
        '<li class="list-group-item">Price: <b>' + cenaKursa + '$</b></li>' +
        '<li class="list-group-item">Lections: <b>' + brojLekcijaKursa + '</b></li></li>' +
        '<li class="list-group-item">Category: <b>' + kategorijaKursa + '</b></li>' +
        '<li class="list-group-item">Language: <b>' + jezikKursa + '</b></li>' +
        '<li class="list-group-item"><b><p><span style="color: rgb(4, 150, 4);">' + brojKorisnikaKursa + '</span> people passed the course</p></b></li>' +
        '<li class="list-group-item"><i class="far fa-star"></i>Avg rating: <b>' + prosecnaOcenaKursa + '</b></li>' +
        '<li class="list-group-item"><b> Sertified: ' + sertifikovanKurs + '</b></li></ul>'
}

function kursDetaljiGenerisanje(i) {
    var detaljiArr = '';
    document.getElementById('gen-kurs-detalji').innerHTML = '';

    var kurseviArr = [
        kurseviDetaljiBaza[Object.keys(kurseviDetaljiBaza)[i]].slika,
        kurseviDetaljiBaza[Object.keys(kurseviDetaljiBaza)[i]].naziv,
        kurseviDetaljiBaza[Object.keys(kurseviDetaljiBaza)[i]].opis,
        kurseviDetaljiBaza[Object.keys(kurseviDetaljiBaza)[i]].autor,
        kurseviDetaljiBaza[Object.keys(kurseviDetaljiBaza)[i]].id,
        kurseviDetaljiBaza[Object.keys(kurseviDetaljiBaza)[i]].datumIzmene,
        kurseviDetaljiBaza[Object.keys(kurseviDetaljiBaza)[i]].cena,
        kurseviDetaljiBaza[Object.keys(kurseviDetaljiBaza)[i]].brojLekcija,
        kurseviDetaljiBaza[Object.keys(kurseviDetaljiBaza)[i]].kategorija,
        kurseviDetaljiBaza[Object.keys(kurseviDetaljiBaza)[i]].jezik,
        kurseviDetaljiBaza[Object.keys(kurseviDetaljiBaza)[i]].brojKorisnika,
        kurseviDetaljiBaza[Object.keys(kurseviDetaljiBaza)[i]].prosecnaOcena,
        kurseviDetaljiBaza[Object.keys(kurseviDetaljiBaza)[i]].sertifikovan
    ]
    detaljiArr = kursDetaljiKartica(kurseviArr[0], kurseviArr[1], kurseviArr[2], kurseviArr[3], kurseviArr[4], kurseviArr[5],
        kurseviArr[6], kurseviArr[7], kurseviArr[8], kurseviArr[9], kurseviArr[10], kurseviArr[11], kurseviArr[12]);
    document.getElementById("gen-kurs-detalji").innerHTML = detaljiArr
}

function obrisiKurs() {
    if (confirm("Obrisi kurs?")) {
        window.open("kursevi.html", "_self");
        alert("Uspesno ste obrisali kurs");
    }
}

function izmeneKursaPozivanje(i) {
    window.open("kursevi_izmene.html?indeksKarticeKursa=" + i, "_self");
}

function dodajKursUKorpu(indeks) {
    indeks = Number(indeks);
    window.open("shopping-kart.html?indeksKorpa=" + indeks, "_self");
    brojIndekse();
}

