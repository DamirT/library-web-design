var baza = 'https://sf-12-2018-default-rtdb.firebaseio.com/korisnici.json';
//var poljaIzmeneKorisnika = document.getElementById("polja-izmene-korisnika");
var korisniciIzmeneBaza;

function korisniciIzmenePodaci(i) {
    var request = new XMLHttpRequest();

    request.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (this.status == 200) {
                korisniciIzmeneBaza = JSON.parse(this.responseText);
                korisniciIzmeneGenerisanje(i)
            } else {
                alert('Greska prilikom ucitavanja podataka.');
            }
        }
    }

    request.open('GET', baza);
    request.send();
}

function karticaKorisnikIzmene(imeKorisnika, prezimeKorisnika, emailKorisnika, korisnickoImeKorisnika, datumRodjenjaKorisnika, adresaKorisnika, telefonKorisnika, lozinkaKorisnika) {

    return '<div class="form-group"><label for="name">Name</label><input type="text" class="form-control" id="nameUser" value="' +
        imeKorisnika + '"></div><div class="form-group"><label for="surname">Surname</label><input type="text" class="form-control" id="surnameUser" value="' +
        prezimeKorisnika + '"></div><div class="form-group"><label for="email">Email address</label><input type="text" class="form-control" id="emailUser" aria-describedby="emailHelp" value="' +
        emailKorisnika + '"></div><div class="form-group"><label for="userName">User name</label><input type="text" class="form-control" id="userNameUser" value="' +
        korisnickoImeKorisnika + '"></div><div class="form-group"><label for="birthday">Date of birth</label><input type="date" class="form-control" id="birthdayUser" value="' +
        datumRodjenjaKorisnika + '"></div><div class="form-group"><label for="adress">Adress</label><input type="text" class="form-control" id="adressUser" value="' +
        adresaKorisnika + '"></div><div class="form-group"><label for="phone">Phone number</label><input type="text" class="form-control" id="phoneUser" value="' +
        telefonKorisnika + '"></div><div class="form-group"><label for="password">Password</label><input type="password" class="form-control" id="passwordUser" value="' +
        lozinkaKorisnika + '"></div>'
}

function korisniciIzmeneGenerisanje(i) {
    var izmeneArr = '';
    document.getElementById("polja-izmene-korisnika").innerHTML = '';
    var korisnikArr = [
        korisniciIzmeneBaza[Object.keys(korisniciIzmeneBaza)[i]].ime,
        korisniciIzmeneBaza[Object.keys(korisniciIzmeneBaza)[i]].prezime,
        korisniciIzmeneBaza[Object.keys(korisniciIzmeneBaza)[i]].email,
        korisniciIzmeneBaza[Object.keys(korisniciIzmeneBaza)[i]].korisnickoIme,
        korisniciIzmeneBaza[Object.keys(korisniciIzmeneBaza)[i]].datumRodjenja,
        korisniciIzmeneBaza[Object.keys(korisniciIzmeneBaza)[i]].adresa,
        korisniciIzmeneBaza[Object.keys(korisniciIzmeneBaza)[i]].telefon,
        korisniciIzmeneBaza[Object.keys(korisniciIzmeneBaza)[i]].lozinka
    ]
    izmeneArr = karticaKorisnikIzmene(korisnikArr[0], korisnikArr[1], korisnikArr[2], korisnikArr[3], korisnikArr[4],
        korisnikArr[5], korisnikArr[6], korisnikArr[7]);
    document.getElementById("polja-izmene-korisnika").innerHTML = izmeneArr
}


function proslediIzmeneKorisnika() {
    var ime = document.getElementById('nameUser').value;
    var prezime = document.getElementById('surnameUser').value;
    var email = document.getElementById('emailUser').value;
    var korisnickoIme = document.getElementById('userNameUser').value;
    var datumRodjenja = document.getElementById('birthdayUser').value;
    var adresa = document.getElementById('adressUser').value;
    var kontakt = document.getElementById('phoneUser').value;
    var sifra = document.getElementById('passwordUser').value;

    var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
    if(ime == '' || prezime == '' || email == '' || korisnickoIme == '' || datumRodjenja == '' || adresa == '' || kontakt == '' || sifra == '') {
        alert("Nisu uneti svi podaci");
    } else if (!email.match(validRegex)) {
        alert("email nije unet pravilno");
    } else {
        alert("Uspesno ste izmenili korisnika");
        window.open('korisnici.html', "_self");
    }
    
}