var baza = 'https://sf-12-2018-default-rtdb.firebaseio.com/korisnici.json';
var korisniciBaza

function korisniciPodaci() {
    var request = new XMLHttpRequest();

    request.onreadystatechange = function () {
        if (this.readyState == 4) {
            if (this.status == 200) {
                korisniciBaza = JSON.parse(this.responseText);
                korisniciGenerisanje(Object.keys(korisniciBaza).length)
            } else {
                alert('Greska prilikom ucitavanja podataka.');
            }
        }
    }

    request.open('GET', baza);
    request.send();
}

function korisnikKartica(imeKorisnika, prezimeKorisnika, emailKorisnika, korisnickoImeKorisnika, indexKorisnika) {
    return '<div class="col-md-2 col-sm-12"><div class="card" style="width: 14rem;"><div class="card-header"><i class="fas fa-user fa-2x"></i></div>' +
        '<ul class="list-group list-group-flush"><li class="list-group-item">Name: <b>' +
        imeKorisnika + '</b></li><li class="list-group-item">Surname: <b>' +
        prezimeKorisnika + '</b></li><li class="list-group-item">Email: <b>' +
        emailKorisnika + '</b></li><li class="list-group-item">User name: <b>' +
        korisnickoImeKorisnika + '</b></li></ul><button class = "btn btn-primary"  onclick="detaljiKorisnikaPozivanje(' + indexKorisnika + ')"> Details </button></div></div>'
}

function korisniciGenerisanje(brojKorisnika) {
    var usersArr = '';
    var itr = 0
    document.getElementById("cards-users").innerHTML = ''
    for (var i = 0; i < brojKorisnika; i++) {
        var korisniciArr = [
            korisniciBaza[Object.keys(korisniciBaza)[i]].ime,
            korisniciBaza[Object.keys(korisniciBaza)[i]].prezime,
            korisniciBaza[Object.keys(korisniciBaza)[i]].email,
            korisniciBaza[Object.keys(korisniciBaza)[i]].korisnickoIme
        ]
        usersArr += korisnikKartica(korisniciArr[0], korisniciArr[1], korisniciArr[2], korisniciArr[3], i);
        itr++;
        if (itr == 6 || i == (brojKorisnika - 1)) {
            document.getElementById("cards-users").innerHTML += '<div class="row">' + usersArr + '</div>'
            usersArr = '';
            itr = 0
        }
    }
}

function detaljiKorisnikaPozivanje(i) {
    window.open("korisnici_detalji.html?indeksKorisnikaKursa=" + i, "_self");
}